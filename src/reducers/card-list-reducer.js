import {
  FETCH_CARD_LIST_ERROR,
  FETCH_CARD_LIST_START,
  FETCH_CARD_LIST_SUCCESS,
  SET_CARD_LIST
} from '../actions';

const defaultCardsState = {
  payload: [],
  err: null,
  isLoading: false,
};

const cardListReducer = (state = defaultCardsState, action) => {
  switch (action.type) {
    case FETCH_CARD_LIST_START:
      return {
        payload: [],
        err: null,
        isLoading: true,
      };

    case FETCH_CARD_LIST_SUCCESS:
      return {
        payload: action.payload,
        err: null,
        isLoading: false,
      }

    case FETCH_CARD_LIST_ERROR:
      return {
        payload: [],
        err: action.err,
        isLoading: false,
      }

    case SET_CARD_LIST:
      return {
        ...state,
        payload: action.payload,
      }

    default:
      return state;
  }
}

export default cardListReducer;
