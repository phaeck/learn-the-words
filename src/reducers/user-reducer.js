import { ADD_USER, CLEAR_USER } from '../actions';

const userReducer = (state = { uid: null }, action) => {
  switch (action.type) {
    case ADD_USER:
      return {
        uid: action.user.uid,
        name: action.user.displayName,
        email: action.user.email,
      };

    case CLEAR_USER:
      return {
        uid: false,
        name: '',
        email: ''
      };

    default:
      return state;
  }
}

export default userReducer;
