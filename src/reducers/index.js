import { combineReducers } from 'redux';
import userReducer from './user-reducer';
import cardListReducer from './card-list-reducer';
import editCardReducer from './edit-card-reducer';

export default combineReducers({
  user: userReducer,
  cardList: cardListReducer,
  editCard: editCardReducer,
});
