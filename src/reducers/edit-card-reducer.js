import {
  EDIT_ENGLISH_WORD,
  EDIT_RUSSIAN_WORD,
  FETCH_CARD_ERROR,
  FETCH_CARD_START,
  FETCH_CARD_SUCCESS,
  SAVE_CARD,
  UPDATE_ENGLISH_WORD,
  UPDATE_RUSSIAN_WORD
} from '../actions';

const defaultEditCardState = {
  card: null,
  isCardLoading: false,
  isRusEditing: false,
  isEngEditing: false,
  loadingError: null
};

const editCardReducer = (state = defaultEditCardState, action) => {
  switch (action.type) {
    case FETCH_CARD_START:
      return {
        ...state,
        isCardLoading: true,
      }

    case FETCH_CARD_SUCCESS:
      return {
        ...state,
        card: action.payload,
        isCardLoading: false,
      }

    case FETCH_CARD_ERROR:
      return {
        ...state,
        card: null,
        loadingError: action.err,
        isCardLoading: false,
      }

    case EDIT_ENGLISH_WORD:
      return {
        ...state,
        isEngEditing: true,
      }

    case UPDATE_ENGLISH_WORD:
      return {
        ...state,
        card: {
          ...state.card,
          eng: action.payload,
        }
      }

    case EDIT_RUSSIAN_WORD:
      return {
        ...state,
        isRusEditing: true,
      }

    case UPDATE_RUSSIAN_WORD:
      return {
        ...state,
        card: {
          ...state.card,
          rus: action.payload,
        }
      }

    case SAVE_CARD:
      return {
        ...state,
        isRusEditing: false,
        isEngEditing: false,
        card: action.payload,
      }

    default:
      return state;
  }
}

export default editCardReducer;
