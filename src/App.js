import React, { PureComponent } from 'react';
import { Redirect, Route, Switch, withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import s from './App.module.scss';
import SpinnerComponent from './components/Spinner';
import FirebaseContext from './contexts/firebase';
import { PrivateRoute } from './utils/private-route';
import HomePage from './pages/Home';
import LoginPage from './pages/Login';
import RegistrationPage from './pages/Registration';
import HeaderComponent from './components/HeaderComponent';
import FooterComponent from './components/FooterComponent';
import EditCardPage from './pages/EditCard';
import WelcomeText from './components/WelcomeText';
import { addUserAction, clearUserAction } from './actions';

class App extends PureComponent {
  _isMounted = false;

  get username() {
    const { userEmail } = this.props;
    return userEmail ? userEmail.split('@')[0] : '';
  }

  componentDidMount() {
    this._isMounted = true;
    const { auth, setUserUid } = this.context;
    const { addUser, clearUser } = this.props;

    auth.onAuthStateChanged(user => {
      if (this._isMounted) {
        if (user) {
          setUserUid(user.uid);
          localStorage.setItem('user', JSON.stringify(user.uid));
          addUser(user);
        } else {
          setUserUid(null);
          localStorage.removeItem('user');
          clearUser();
        }
      }
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    const { userUid } = this.props;

    if (userUid === null) {
      return (
        <div className={ s.loader }>
          <SpinnerComponent text={ 'Loading...' }/>
        </div>
      )
    }

    return (
      <Switch>
        <Route path="/login" component={ LoginPage }/>
        <Route path="/registration" component={ RegistrationPage }/>
        <Route render={ () => {
          return (
            <>
              <HeaderComponent username={ this.username }/>
              <div className={ s.mainContent }>
                <Switch>
                  <Route exact path="/" component={ WelcomeText }/>
                  <PrivateRoute path="/cards" component={ HomePage }/>
                  <PrivateRoute path="/edit/:id" component={ EditCardPage }/>
                  <Redirect to={ userUid ? '/cards' : '/' }/>
                </Switch>
              </div>
              <FooterComponent/>
            </>
          );
        } }/>
        <Redirect to={ userUid ? '/cards' : '/' }/>
      </Switch>
    )
  }
}

App.contextType = FirebaseContext;

const mapStateToProps = (state) => {
  return {
    userUid: state.user.uid,
    userEmail: state.user.email,
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    addUser: addUserAction,
    clearUser: clearUserAction,
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(App));
