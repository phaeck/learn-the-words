import React, { PureComponent } from 'react';
import cl from 'classnames';

import s from './FlipCard.module.scss';
import { CheckSquareOutlined, DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { withRouter } from 'react-router-dom';

class FlipCard extends PureComponent {
  state = {
    isRotated: false,
    isRemembered: false,
  }

  rotateCard = () => {
    if (!this.state.isRemembered) {
      this.setState(({ isRotated }) => ({ isRotated: !isRotated }));
    }
  }

  rememberCard = () => {
    if (!this.state.isRemembered) {
      this.setState(() => ({
        isRemembered: true,
        isRotated: false,
      }));
    }
  }

  render() {
    const { eng, rus, onDeleteCard, index, history: { push } } = this.props;
    const { isRotated, isRemembered } = this.state;

    return (
      <div className={ s.container }>
        <div className={ cl(s.card, {
          [s.rotated]: isRotated,
          [s.remembered]: isRemembered,
        }) }>
          <div className={ s.cardInner }
               onClick={ this.rotateCard }>
            <div className={ s.cardFront }>
              { eng }
            </div>
            <div className={ s.cardBack }>
              { rus }
            </div>
          </div>
        </div>

        <div className={ s.iconContainer }>
          <div className={ cl(s.icon, s.remember) }
               onClick={ this.rememberCard }>
            <CheckSquareOutlined/>
          </div>
          <div className={ cl(s.icon, s.edit) }
               onClick={ () => push(`edit/${ index }`) }>
            <EditOutlined/>
          </div>
          <div className={ cl(s.icon, s.delete) }
               onClick={ onDeleteCard }>
            <DeleteOutlined/>
          </div>
        </div>

      </div>
    );
  }
}

export default withRouter(FlipCard);
