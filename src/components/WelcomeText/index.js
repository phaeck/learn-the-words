import React, { PureComponent } from 'react';
import { withRouter } from 'react-router-dom';
import s from './WelcomeText.module.scss';

class WelcomeText extends PureComponent {
  componentDidMount() {
    const { history: { push } } = this.props;
    if (localStorage.getItem('user')) {
      push('/cards');
    }
  }

  render() {
    return (
      <div className={ s.text }>
        {/*eslint-disable-next-line*/ }
        <h1>Hi there! 😊</h1>
        <p>For start, click "login" button!</p>
      </div>
    )
  }
}

export default withRouter(WelcomeText);
