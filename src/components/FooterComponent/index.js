import React from 'react';

import s from './FooterComponent.module.scss';

const FooterComponent = () => {
  return (
    <div className={s.footer}>
      phaeck's react marathon 2020
    </div>
  );
}

export default FooterComponent;
