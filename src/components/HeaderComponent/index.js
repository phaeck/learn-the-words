import React, { PureComponent } from 'react';

import s from './HeaderComponent.module.scss';
import ReactLogo from './img/logo.png';
import UserOutlined from '@ant-design/icons/lib/icons/UserOutlined';
import FirebaseContext from '../../contexts/firebase';
import { LoginOutlined, LogoutOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';

class HeaderComponent extends PureComponent {
  state = {
    isLogoutHovered: false,
  };

  render() {
    const { isLogoutHovered } = this.state;
    const { username } = this.props;
    const { logout } = this.context;

    return (
      <div className={ s.header }>
        <div className={ s.logoWrapper }>
          <img className={ s.logo } src={ ReactLogo } alt="react logo"/>
          <span className={ s.title }>Learn the words app</span>
        </div>
        { username &&
        <button className={ s.loginBtn }
                onClick={ logout }
                onMouseEnter={ () => this.setState({ isLogoutHovered: true }) }
                onMouseLeave={ () => this.setState({ isLogoutHovered: false }) }>

          { isLogoutHovered
            ? <>
              <LogoutOutlined className={ s.icon }/>
              Logout
            </>
            : <>
              <UserOutlined className={ s.icon }/>
              { username }
            </> }
        </button> }

        { !username && <Link to="/login"
                             className={ s.loginBtn }>
          <LoginOutlined className={ s.icon }/>
          Login
        </Link> }
      </div>
    );
  }
}

HeaderComponent.contextType = FirebaseContext;

export default HeaderComponent;
