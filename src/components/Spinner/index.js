import React from 'react';
import {LoadingOutlined} from '@ant-design/icons';
import {Spin} from 'antd';
import cl from 'classnames';
import s from './Spinner.module.scss';

const SpinnerComponent = ({ text = '', loaderSize = 48, isTextLight = false, fontSize = 24}) => {
  const loader = <LoadingOutlined style={{ fontSize: loaderSize, color: isTextLight ? '#eafffc' : '#0d4f8d' }} spin />

  return (
    <div className={s.container}>
      <Spin indicator={loader} />
      {text && <div className={cl(s.text, { [s.light]: isTextLight })}
           style={{ fontSize }}>{text}
      </div>}
    </div>
  )
};

export default SpinnerComponent;
