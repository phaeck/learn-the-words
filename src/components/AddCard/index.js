import React, {Component} from 'react';
import s from './AddCard.module.scss';
import {Button, Input, message} from 'antd';
import {getTranslate} from '../../services/dictionary';

const { Search } = Input;

class AddCard extends Component {
  state = {
    value: '',
    english: '',
    translated: '',
    isTranslateLoading: false,
    isTranslateSending: false,
  };

  getTheWord = async () => {
    try {
      const { value } = this.state;
      const { text, translate } = await getTranslate(value);
      this.setState({
        english: text,
        translated: translate,
        isTranslateLoading: false,
      });
    } catch (e) {
      message.error(e.message);
      this.setState({ isTranslateLoading: false });
    }
  }

  handleSearch = async () => {
    this.setState({
      isTranslateLoading: true,
    }, this.getTheWord)
  }

  handleSearchChange = ({target}) => this.setState({value: target.value});

  addWordToDatabase = () => {
    const { english, translated } = this.state;
    this.props.addNewCard(english, translated);
    this.setState({
      value: '',
      english: '',
      translated: '',
      isTranslateSending: false,
    });
  }

  handleAddWord = async () => {
    this.setState({
      isTranslateSending: true
    }, this.addWordToDatabase);
  }

  render() {
    const {isTranslateLoading, isTranslateSending, value, english, translated} = this.state;

    return <div className={s.container}>
      <div className={s.text}>For add new card get translate from english, then click "Add new card"!</div>
      <Search placeholder="Type english word"
              value={value}
              loading={isTranslateLoading}
              onChange={this.handleSearchChange}
              onSearch={this.handleSearch}
              enterButton />

      {translated && <div className={s.words}>
        <span className={s.word}>{english}:</span>
        <span className={s.word}>{translated}</span>
        <Button type="default"
                loading={isTranslateSending}
                onClick={this.handleAddWord}>Add new card</Button>
      </div>}
    </div>
  }
}

export default AddCard;
