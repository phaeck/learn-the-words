import React, { Component } from 'react';

import s from './CardList.module.scss';
import FlipCard from '../FlipCard';
import AddCard from '../AddCard';
import SpinnerComponent from '../Spinner';

class CardList extends Component {
  render() {
    const { cards, newCardAdded, cardDeleted, isCardsLoading } = this.props;

    return (
      <div className={ s.container }>
        <AddCard addNewCard={ newCardAdded }/>
        { !isCardsLoading && cards.length && <div className={ s.cardsList }>
          { cards.map(({ id, eng, rus }, index) =>
            <FlipCard key={ id }
                      eng={ eng }
                      rus={ rus }
                      index={ index }
                      onDeleteCard={ () => cardDeleted(id) }
            />) }
        </div> }

        { !isCardsLoading && !cards.length && <div className={ s.emptyText }>
          There is no cards in database!
        </div> }

        { isCardsLoading && <div className={ s.loader }>
          <SpinnerComponent text={ 'loading cards...' }
                            loaderSize={ 30 }
                            fontSize={ 16 }
                            isTextLight={ true }/>
        </div> }
      </div>
    );
  }
}

export default CardList;
