import React, {PureComponent} from 'react';

import s from './UserForm.module.scss';
import {Button, Form, Input} from 'antd';

class UserForm extends PureComponent {
  render() {
    const { title, onSubmit, buttonText } = this.props;

    const layout = {
      labelCol: { span: 8 },
      wrapperCol: { span: 16 },
    };
    const tailLayout = {
      wrapperCol: { offset: 8, span: 16 },
    };

    return (<div className={s.container}>
      <div className={s.title}>{title}</div>
      <Form
        {...layout}
        name="basic"
        initialValues={{ remember: true }}
        onFinish={({email, password}) => onSubmit(email, password)}
      >
        <Form.Item
          label="Email"
          name="email"
          rules={[{ required: true, message: 'Please input your email!' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[{ required: true, message: 'Please input your password!' }]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            {buttonText}
          </Button>
        </Form.Item>
      </Form>
    </div>)
  }
}

export default UserForm;
