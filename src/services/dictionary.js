const apiKey = process.env.REACT_APP_TRANSLATE_API_KEY;

export const getTranslate = async (text, language = 'en-ru') => {
  const res = await fetch(`https://reactmarathon-api.netlify.app/api/translate?text=${text}&lang=${language}`, {
    headers: {
      'Authorization': apiKey
    }
  });
  if (res.status === 400) {
    throw Error(await res.json().then(({message}) => message));
  }
  return res.json();
}
