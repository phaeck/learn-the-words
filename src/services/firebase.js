import * as firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';

const {
  REACT_APP_API_KEY: apiKey,
  REACT_APP_AUTH_DOMAIN: authDomain,
  REACT_APP_DATABASE_URL: databaseURL,
  REACT_APP_PROJECT_ID: projectId,
  REACT_APP_STORAGE_BUCKET: storageBucket,
  REACT_APP_MESSAGE_SENDER_ID: messagingSenderId,
  REACT_APP_APP_ID: appId,
} = process.env;


class Firebase {
  constructor() {
    firebase.initializeApp({
      apiKey,
      authDomain,
      databaseURL,
      projectId,
      storageBucket,
      messagingSenderId,
      appId
    });

    this.auth = firebase.auth();
    this.database = firebase.database();

    this.userUid = null;
  }

  get cardsUrl() {
    return '/cards/' + this.userUid;
  }

  setUserUid = (uid) => this.userUid = uid;

  loginWithEmail = (email, password) => this.auth.signInWithEmailAndPassword(email, password);

  addUserWithEmail = (email, password) => this.auth.createUserWithEmailAndPassword(email, password);

  logout = () => this.auth.signOut();

  getUserCardsRef = () => this.database.ref(this.cardsUrl);

  getUserCardRefByIndex = (id) => this.database.ref(`${ this.cardsUrl }/${ id }`);

  setUserCards = (cards) => this.database.ref(this.cardsUrl).set(cards);
}

export default Firebase;
