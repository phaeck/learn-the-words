import { ADD_USER, CLEAR_USER } from './action-types';

export const addUserAction = (user) => ({
  type: ADD_USER,
  user,
});

export const clearUserAction = () => ({
  type: CLEAR_USER,
});
