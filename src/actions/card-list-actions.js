import {
  FETCH_CARD_LIST_ERROR,
  FETCH_CARD_LIST_START,
  FETCH_CARD_LIST_SUCCESS,
  SET_CARD_LIST
} from './action-types';

export const fetchCardList = (getData) => {
  return (dispatch) => {
    dispatch(fetchCardListStartAction());
    getData().once('value').then(res => {
      dispatch(fetchCardListSuccessAction(res.val() || []));
    }).catch(err => {
      dispatch(fetchCardListErrorAction(err));
    });
  }
}

export const setCardList = (setData, cards) => {
  return dispatch => {
    dispatch(setCardListAction(cards));
    setData(cards);
  }
}

export const fetchCardListStartAction = () => ({
  type: FETCH_CARD_LIST_START,
});

export const fetchCardListSuccessAction = (payload) => ({
  type: FETCH_CARD_LIST_SUCCESS,
  payload
});

export const fetchCardListErrorAction = (err) => ({
  type: FETCH_CARD_LIST_ERROR,
  err
});

export const setCardListAction = (payload) => ({
  type: SET_CARD_LIST,
  payload
});
