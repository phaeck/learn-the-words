import {
  EDIT_ENGLISH_WORD,
  EDIT_RUSSIAN_WORD,
  FETCH_CARD_ERROR,
  FETCH_CARD_START,
  FETCH_CARD_SUCCESS,
  SAVE_CARD,
  UPDATE_ENGLISH_WORD,
  UPDATE_RUSSIAN_WORD
} from './action-types';
import { setCardList } from './card-list-actions';


export const fetchCard = (getData, id) => {
  return (dispatch) => {
    dispatch(fetchCardStartAction());

    getData(id).once('value').then(res => {
      dispatch(fetchCardSuccessAction(res.val() || {}));
    }).catch(err => {
      dispatch(fetchCardErrorAction(err));
    });
  }
}

export const saveCard = (getData, setData, card) => {
  return dispatch => {
    getData().once('value').then(res => {
      const cards = res.val() || [];
      const editedCardIndex = cards.findIndex(c => c.id === card.id);
      if (editedCardIndex !== -1) {
        cards[editedCardIndex] = card;

        setData(cards).then(() => dispatch(saveCardAction(card)));
      }
    });
  }
}

export const deleteCard = (getData, setData, cardId, push) => {
  return (dispatch) => {
    getData().once('value').then(res => {
      const cards = res.val() || [];
      const newCards = cards.filter(c => c.id !== cardId);

      setCardList(setData, newCards)(dispatch);
      push('/cards');
    });
  }
}

export const fetchCardStartAction = () => ({
  type: FETCH_CARD_START
});

export const fetchCardSuccessAction = (payload) => ({
  type: FETCH_CARD_SUCCESS,
  payload
});

export const fetchCardErrorAction = (err) => ({
  type: FETCH_CARD_ERROR,
  err
})

export const editEnglishWordAction = () => ({
  type: EDIT_ENGLISH_WORD
});

export const updateEnglishWordAction = (payload) => ({
  type: UPDATE_ENGLISH_WORD,
  payload
});

export const editRussianWordAction = () => ({
  type: EDIT_RUSSIAN_WORD
});

export const updateRussianWordAction = (payload) => ({
  type: UPDATE_RUSSIAN_WORD,
  payload
});

export const saveCardAction = (payload) => ({
  type: SAVE_CARD,
  payload
});
