import React, { Component } from 'react';

import s from './Registration.module.scss';
import UserForm from '../../components/UserForm';
import { Button, message } from 'antd';
import FirebaseContext from '../../contexts/firebase';
import { Link } from 'react-router-dom';

class RegistrationPage extends Component {
  signUp = (email, password) => {
    const { history: { push } } = this.props;
    const { addUserWithEmail, setUserUid } = this.context;

    if (!email || !password) {
      message.error('Fill empty fields');
      return;
    }

    addUserWithEmail(email, password)
      .then(({ user }) => {
        setUserUid(user.uid);
        message.success(`User ${ user.email } created successfully!`);
        push('/cards');
      })
      .catch((err) => message.error(err.message));
  }

  render() {
    return (
      <div className={ s.container }>
        <UserForm title={ 'Registration' }
                  buttonText={ 'Sign up' }
                  onSubmit={ this.signUp }
        />

        <Link to="/login">
          <Button className={ s.button }
                  type={ 'default' }>
            Already have login?
          </Button>
        </Link>
      </div>
    );
  }
}

RegistrationPage.contextType = FirebaseContext;

export default RegistrationPage;
