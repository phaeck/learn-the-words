import React, { PureComponent } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Button, Input, message } from 'antd';
import { bindActionCreators } from 'redux';
import { ArrowLeftOutlined, CheckSquareOutlined, DeleteOutlined, EditOutlined } from '@ant-design/icons';

import s from './EditCard.module.scss';
import FirebaseContext from '../../contexts/firebase';
import SpinnerComponent from '../../components/Spinner';
import {
  deleteCard,
  editEnglishWordAction,
  editRussianWordAction,
  fetchCard,
  saveCard,
  updateEnglishWordAction,
  updateRussianWordAction
} from '../../actions';

class EditCardPage extends PureComponent {
  componentDidMount() {
    const { match: { params }, fetchCard } = this.props;
    const { getUserCardRefByIndex } = this.context;

    fetchCard(getUserCardRefByIndex, +params.id);
  }

  saveCard = () => {
    const { card, saveCard } = this.props;
    const { getUserCardsRef, setUserCards } = this.context;

    if (!card.eng || !card.rus) {
      message.error('Fields can\'t be empty!');
      return;
    }

    saveCard(getUserCardsRef, setUserCards, card);
  }

  deleteCard = () => {
    const { getUserCardsRef, setUserCards } = this.context;
    const { history: { push }, card, deleteCard } = this.props;

    deleteCard(getUserCardsRef, setUserCards, card.id, push);
  }

  render() {
    const {
      isCardLoading,
      card,
      isRusEditing,
      isEngEditing,
      editRusWord,
      editEngWord,
      updateRusWord,
      updateEngWord
    } = this.props;
    const { match: { params }, history: { push } } = this.props;

    return (
      <div className={ s.container }>
        { isCardLoading && <div className={ s.loader }>
          <SpinnerComponent text={ 'loading card...' }
                            loaderSize={ 30 }
                            fontSize={ 16 }
                            isTextLight={ true }
          />
        </div> }

        { !isCardLoading && !card && <div className={ s.text }>
          There is no card with index { params.id }!
        </div> }

        { !isCardLoading && card && <>
          <div className={ s.card }>
            { !isEngEditing && <div className={ s.word }>
              <span>{ card.eng }</span>
              <div className={ s.edit }
                   onClick={ () => editEngWord() }
              >
                <EditOutlined/>
              </div>
            </div> }
            { isEngEditing &&
            <Input className={ s.input }
                   placeholder="Type english word"
                   value={ card.eng }
                   onChange={ ({ target }) => updateEngWord(target.value) }
            /> }

            { !isRusEditing && <div className={ s.word }>
              <span>{ card.rus }</span>
              <div className={ s.edit }
                   onClick={ () => editRusWord() }
              >
                <EditOutlined/>
              </div>
            </div> }

            { isRusEditing &&
            <Input className={ s.input }
                   placeholder="Type russian word"
                   value={ card.rus }
                   onChange={ ({ target }) => updateRusWord(target.value) }
            />
            }
          </div>

          <div className={ s.actions }>
            <Button type="default"
                    onClick={ () => push('/cards') }
            >
              <ArrowLeftOutlined/>
              Back
            </Button>
            <Button type="default"
                    onClick={ this.saveCard }
            >
              <CheckSquareOutlined/>
              Save
            </Button>
            <Button type="default"
                    onClick={ this.deleteCard }
            >
              <DeleteOutlined/>
              Delete
            </Button>
          </div>
        </> }
      </div>
    );
  }
}

EditCardPage.contextType = FirebaseContext;

const mapStateToProps = (state) => ({
  card: state.editCard.card,
  isEngEditing: state.editCard.isEngEditing,
  isRusEditing: state.editCard.isRusEditing,
  isCardLoading: state.editCard.isCardLoading
});

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    fetchCard,
    saveCard,
    deleteCard,
    editRusWord: editRussianWordAction,
    editEngWord: editEnglishWordAction,
    updateRusWord: updateRussianWordAction,
    updateEngWord: updateEnglishWordAction,
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(EditCardPage));
