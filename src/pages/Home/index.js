import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import s from './Home.module.scss';
import CardList from '../../components/CardList';
import FirebaseContext from '../../contexts/firebase';
import { fetchCardList, setCardList } from '../../actions';

class HomePage extends PureComponent {
  componentDidMount() {
    const { getUserCardsRef } = this.context;
    const { fetchCardList } = this.props;

    fetchCardList(getUserCardsRef);
  }

  onNewCardAdded = (english, translated) => {
    const { cards, setCardList } = this.props;
    const { setUserCards } = this.context;

    const newCards = [
      ...cards,
      { eng: english, rus: translated, id: Date.now() }
    ];

    setCardList(setUserCards, newCards);
  };

  onCardDeleted = (id) => {
    const { cards, setCardList } = this.props;
    const { setUserCards } = this.context;

    const newCards = cards.filter(card => card.id !== id);

    setCardList(setUserCards, newCards);
  };

  render() {
    const { cards, isLoading } = this.props;
    return (
      <>
        <div className={ s.text }>
          Click on card for open translation on Russian
        </div>
        <CardList cards={ cards }
                  isCardsLoading={ isLoading }
                  newCardAdded={ this.onNewCardAdded }
                  cardDeleted={ this.onCardDeleted }/>
      </>
    );
  }
}

HomePage.contextType = FirebaseContext;

const mapStateToProps = (state) => ({
  cards: state.cardList.payload,
  isLoading: state.cardList.isLoading,
});

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    fetchCardList,
    setCardList
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
