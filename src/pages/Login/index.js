import React, { Component } from 'react';

import s from './Login.module.scss';
import UserForm from '../../components/UserForm';
import { Button, message } from 'antd';
import FirebaseContext from '../../contexts/firebase';
import { Link } from 'react-router-dom';

class LoginPage extends Component {
  login = (email, password) => {
    const username = email.split('@')[0];
    const { loginWithEmail, setUserUid } = this.context;
    const { history: { push } } = this.props;

    loginWithEmail(email, password)
      .then(({ user }) => {
        setUserUid(user.uid);
        localStorage.setItem('user', JSON.stringify(user.uid));
        message.success(`Welcome, ${ username }!`);
        push('/cards');
      })
      .catch(err => message.error(err.message));
  }

  render() {
    return (
      <div className={ s.container }>
        <UserForm title={ 'Login' }
                  buttonText={ 'Login' }
                  onSubmit={ this.login }
        />

        <Link to="/registration">
          <Button className={ s.button }
                  type={ 'default' }>
            Not registered?
          </Button>
        </Link>
      </div>
    );
  }
}

LoginPage.contextType = FirebaseContext;

export default LoginPage;
